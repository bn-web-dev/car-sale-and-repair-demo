module.exports = router = require('express').Router();
const fs = require('fs');
const path = require('path');
const multer = require('multer');
const imagemin = require('imagemin');
const imageminWebp = require('imagemin-webp');
const uuid = require('uuid/v1')
let jobs = require('./data/jobs.json')
let deletablePaths = require('./data/deletable-paths.json');

const upload = multer({
  storage: multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'public/images/gallery');
    },
    filename: (req, file, cb) => {
      cb(null, `${file.fieldname}-${new Date().toISOString()}${path.extname(file.originalname)}`);
    }
  }),
  fileFilter: function (req, file, cb) {
    const fileTypes = /(jpeg|jpg|png|gif)$/i;
    const extname = fileTypes.test(path.extname(file.originalname));
    const mimetype = fileTypes.test(file.mimetype);
    if (extname && mimetype) {
      cb(null, true);
    } else {
      cb('Please submit an image!', false);
    }
  },
  limits: { fileSize: 1024 * 1024 }
}).array('job-picture');

router.route('/upload')
  .post((req, res) => {
    return upload(req, res, (err) => {
      if (err) {
        if (err instanceof multer.MulterError) {
          if (err.code === 'LIMIT_FILE_SIZE') {
            res.send("The file is too big");
          }
        } else if (typeof err === 'string' || err instanceof String) {
          res.send(err);
        } else {
          res.send("An error occured during the upload");
        }
      } else {
        if (req.files) {
          let job = {
            id: uuid(),
            description: {
              title: req.body.title,
              year: req.body.year,
              make: req.body.make,
              model: req.body.model,
              state: req.body.state,
              condition: req.body.condition,
              price: req.body.price,
              mileage: req.body.mileage,
              extColor: req.body.extColor,
              intColor: req.body.intColor,
              transmission: req.body.transmission,
              drivetrain: req.body.drivetrain,
              availability: req.body.availability
            },
            src: req.files.map(file => {
              return file.path.substring(6, file.path.lastIndexOf('.'))
            }),
            name: req.files.map(file => {
              return file.filename.substring(0, file.filename.lastIndexOf('.'))
            })
          };
          jobs.push(job);
          fs.writeFile('./data/jobs.json', JSON.stringify(jobs), () => { console.log('jobs.json saved'); });
          imagemin(req.files.map(f => f.path), req.files[0].destination, {
            use: [
              imageminWebp({
                quality: 75,
                lossless: false
              })
            ]
          }).then(() => {
            console.log('Images optimized');
          }).catch('Failed to optimize image!');
          res.send(`File successfully uploaded!`);
        } else {
          res.send('No file submitted!');
        }
      }
    })
  })
  .delete((req, res) => {
    var error;
    let deletable = jobs.find(job => {
      return job.id === req.body.id;
    })
    jobs = jobs.filter(job => {
      return job.id !== req.body.id;
    })
    deletable.src.forEach(path => {
      fs.unlink(__dirname + "/public" + path + ".webp", (err, data) => {
        if (err) {
          deletablePaths.push({ path });
          fs.writeFile('./data/deletable-paths.json', JSON.stringify(deletablePaths), () => { console.log('deletable-path.json saved'); });
          console.log(`Failure to delete file "public${path}.webp"`)
        };
      });
      fs.unlink(__dirname + "/public" + path + ".jpg", (err, data) => {
        if (err) {
          deletablePaths.push({ path });
          fs.writeFile('./data/deletable-paths.json', JSON.stringify(deletablePaths), () => { console.log('deletable-path.json saved'); });
          console.log(`Failure to delete file "public${path}.jpg"`)
        };
      });
    });
    fs.writeFile('./data/jobs.json', JSON.stringify(jobs), (err) => {
      if (err) {
        error = true;
        return
      }
      console.log('jobs.json saved');
      error = false
      res.writeHead(200, {
        'Content-Type': 'application/json'
      })
      res.end(JSON.stringify({
        'message': 'Data successfully deleted!',
        jobs
      }));
    });
  })


