module.exports = router = require('express').Router();
let passport = require('passport');

router.route('/login')
    .get((req, res) => {
        res.render('login');
    })
    .post(passport.authenticate('local', {
        successRedirect: '/admin',
        failureRedirect: '/login',
        successFlash: 'Welcome!',
        failureFlash: 'Try Again'
    }));

router.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/login')
})