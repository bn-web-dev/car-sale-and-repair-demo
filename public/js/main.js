/* ----------------------------- General -----------------------------
------------------------------------------------------------------------- */
AOS.init();

/* ----------------------------- Navigation -----------------------------
------------------------------------------------------------------------- */

var imgs = $('img.materialboxed');
var final = _.map(imgs, function(img) {
    return $(img).height();
})
// alert(final)

if (!window.localStorage) {
    $("header").append("<div style='text-align: center; margin: 0; padding: 10px 0px; position: absolute; top: 64px; width: 100%; color: white; background-color: red; z-index: 105;'><h5 style='margin: 0'>Please switch to a browser with a better support for HTML, JavaScript and CSS, Thanks!</h5></div>")
}

document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {
        edge: 'left',
        draggable: true,
        preventScrolling: true
    });
    elems[0].addEventListener('click', function(e) {
        var instance = M.Sidenav.getInstance(this);
        instance.close()
    })
});

// ----- Tabs ------
var ResizeCardsTools = (function () {
    function ResizeCardsTools() { }

    ResizeCardsTools.resizeCards = function (cards) {

        var cardImages = cards.find('.card-image');
        var cardImageImgs = cards.find('img');
        var cardDescriptions = cards.find('.card-content')
        if (cardImages.length) {
            cardImageImgs.css('height', 'auto');
            if (window.innerWidth > 600) {
                cardImages.height(this.resizeEven(cardImageImgs));
            } else {
                cardImages.height(this.resizeEven(cardImageImgs));
            }
        }
        if (cardDescriptions.length) {
            cardDescriptions.css('height', 'auto')
            if (window.innerWidth > 600) {
                this.resizeEven(cardDescriptions);
            } else {
                this.resizeAuto(cardDescriptions);
            }
        }

    }

    ResizeCardsTools.resizeEven = function (elems) {
        var cardsHeights = _.map(elems, function (item) {
            return $(item).height();
        })
        var maxHeight = _.max(cardsHeights);
        var finaleMaxHeight = (maxHeight > 0) ? maxHeight : 170;
        elems.height(finaleMaxHeight);
        return finaleMaxHeight;
    }

    ResizeCardsTools.resizeAuto = function (elems) {
        $(elems).css({ 'height': 'auto' })
    }

    return ResizeCardsTools;
})()

var el = document.getElementById('tabs-swipe')
var instance = M.Tabs.init(el, {
    swipeable: false
});

function menuAction(str) {
    localStorage.setItem('activeTab', str);
    if (localStorage.getItem('activeTab') === 'car-sales') location.reload()
    onPageReload();
    window.scrollTo(0, 0);
}

function onPageReload() {
    var tab = localStorage.getItem('activeTab') || 'home';
    instance.select(tab);
    $('.slides img').css('src', 'none')
    $('#mobile a, #desktop a').removeClass('active');
    switch (tab) {
        case 'home':
            $('#mobile li.home a, #desktop li.home a').addClass('active');
            break;
        case 'about':
            $('#mobile li.about a, #desktop li.about a').addClass('active');
            break;
        case 'services':
            $('#mobile li.services a, #desktop li.services a').addClass('active');
            break;
        case 'car-sales':
            $('#mobile li.gallery a, #desktop li.gallery a').addClass('active');
    }
    var cards = $('#' + tab + ' .card.resizable');
    ResizeCardsTools.resizeCards(cards);
    AOS.init();
    return false;
}

$(document).ready(function() {
    $(window).trigger('resize');
})

$(window).resize(function (e) {
    onPageReload()
})

document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.materialboxed');
    var instances = M.Materialbox.init(elems);
});

/* ------------------------ Home - Welcome Slider -----------------------
------------------------------------------------------------------------- */

document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.slider');
    var instances = M.Slider.init(elems, {
        duration: 500,
        interval: 5000,
        height: 400
    });
});

(function () {
    var img = new Image();
    img.onerror = function () {
        $('.slider img').each(function () {
            var backgroundStr = "background-image: url(" + this.dataset.src + ".jpg)"
            $(this).attr('style', backgroundStr)
        });
        $('body').attr('style', "background: url('images/backgrounds/main-bg.jpg') no-repeat top right / cover");
        $('footer.page-footer').attr('style', "background: url('/images/backgrounds/footer-bg.jpg') no-repeat top right / cover");
    }
    img.src = '/images/slider/slider1.webp'
})();
/* ------------------------------ Modal - Email ------------------------------
------------------------------------------------------------------------- */

var modalInstance;
document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.modal');
    modalInstance = M.Modal.init(elems, {
        startingTop: '1%',
        endingTop: "20%",
        onOpenEnd: function() {
            $('#emailForm').html(`
            <input class="validate" id="email" type="email" placeholder="enter email" name="email"/>
            <textarea id="message" name="message" placeholder="your message"></textarea>
            <button class="waves-effect waves-light btn grey darken-3 btn-flat grey-text" id="send" onclick="sendEmail(event);false;" type="button">Send</button>
            <button class="waves-effect waves-light btn-small grey darken-3 btn-flat grey-text" id="clear" onclick="clearFields(event);false;" type="button">Clear</button>`);
            $email = $('#email');
            $message = $('#message');
            $email.on('change', function () {
                console.log($(this).val())
                localStorage.setItem('email', $(this).val());
            }).on('keyup', function () {
                console.log($(this).val())
                localStorage.setItem('email', $(this).val());
            });
            $message.on('change', function () {
                localStorage.setItem('message', $(this).val());
            }).on('keyup', function () {
                localStorage.setItem('message', $(this).val());
            });
            var email = localStorage.getItem('email');
            var message = localStorage.getItem('message');
            if (email !== "") $email.val(email);
            if (message !== "") $message.val(message);
        },
        onCloseEnd: function() {
            $('#edit-car-details a.modal-load-img').off('click');
            $("#edit-car-details .submit-btn").off('click');

            $("#upload-form .modal-load-img").off('click');
            $("#upload-form .submit-btn").off('click');
        }
    });
});

document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.fixed-action-btn');
    var instances = M.FloatingActionButton.init(elems, {
        direction: 'left'
    });
});

function sendEmail() {
    var email = {
        email: $("#email").val(),
        message: $("#message").val()
    }
    $.post('/mail', email).done(() => {
        $('#emailForm').html('<p>Your message was sent successfully!<br>We will contact you soon.<br>Please give us also a call if you do not hear from us!</p>')
        localStorage.setItem('email', "");
        localStorage.setItem('message', "");
    });
}

function clearFields() {
    $('#email').val(null);
    $('#message').val(null);
    localStorage.setItem('email', "");
    localStorage.setItem('message', "");
}

/* ------------------------ Image Upload Preview ------------------------
------------------------------------------------------------------------- */

var Uploader = (function() {
    debugger;
    cropData = [];
    imagesBuffer = []

    function Uploader(form, previewImages) {
        this.form = form;
        this.previewImages = previewImages;
    }

    Uploader.prototype.post = function(event) {
        var mainDeferred = $.Deferred();
        var data = new FormData(this.form);

        var deferreds = _.map(cropData, cdi => {
            var deferred = $.Deferred();
            cdi.basic.croppie('bind', {
                url: cdi.imgUrl,
                points: cdi.imgPoints
            }).then(function () {
                return cdi.basic.croppie('result', {
                    type: 'blob',
                    size: { width: 500 },
                    format: 'jpeg',
                    quality: 1
                })
            }).then(function (blob) {
                data.append('job-picture', blob, 'image.jpg');
                deferred.resolve(data);
            })
            return deferred;
        })

        $.when.apply($, deferreds).done(function (data) {
            $.ajax({
                type: "post",
                url: '/upload',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function (response) {
                    mainDeferred.resolve(response)
                    cropData = [];
                },
                error: function (response) {
                    mainDeferred.reject(response)
                }
            })
        })

        return mainDeferred;
    }

    Uploader.prototype.delete = function(event, id) {
        var deferred = $.Deferred();
        $.ajax({
            method: "delete",
            url: "/upload",
            data: { "id": id },
            dataType: "json",
            success: function (data) {
                deferred.resolve(data)
            },
            error: function (data) {
                deferred.reject(data)
            }
        });
        return deferred;
    }

    Uploader.prototype.get = function (event) {
        return $.get("/upload/" + $(event.target).closest('.card').find('input.private-data').val())
                .then(function (data) {
                    return car = JSON.parse(data)
                });
    }

    Uploader.prototype.put = function(event, id) {
        event.preventDefault();
        var self = this
        this.post(event)
            .then(function (response) {
                if (response === 'File successfully uploaded!') return self.delete(event, id)
                else return "Updating failed"
            })
            .then(function (response) {
                $('#edit-car-details .fileUploadForm').parent().parent().html("<div class='row container fileUploadForm'><h5>Response</h5><p>The update was successful!</p></div>")
                setTimeout(() => {
                    location.reload();
                }, 500);
            })
    }

    Uploader.prototype.addImgUnit = function (event) {
        var self = this;
        var imgUnit = $("<div class='img-unit center col s12 m12 l6 xl6'></div>");
        var loadImgAnc = $("<a class='modal-upload'>Load</a>");
        var cropLockWrapper = $("<label><input type='checkbox' disabled='disabled' /><span>Lock</span></label>");
        var removeImgAnc = $("<a class='modal-upload'>Remove</a>");
        var uploadInput = $("<input class='upload-input grey darken-3 btn-flat m-1 waves-effect waves-light grey-text' name='job-picture' type='file' />");
        var imgBox = $("<div class='img-box'></div>");
        // imgUnit.append(loadImgAnc);
        imgUnit.append(cropLockWrapper);
        // imgUnit.append(removeImgAnc);
        imgUnit.append(uploadInput);
        imgUnit.append(imgBox);
        this.previewImages.append(imgUnit);


        function addControls() {
            loadImgAnc.insertBefore(cropLockWrapper);
            removeImgAnc.insertAfter(cropLockWrapper)
            loadImgAnc.click(function () {
                uploadInput.click();
            })
            removeImgAnc.click(function () {
                imgUnit.remove()
                self.removeCropData(imgBox)
            })
        }

        addControls()

        var cropLock = cropLockWrapper.find('input');
        this.uploadCarData(imgBox, cropLock, uploadInput, 'change');

        cropLock.click(function (event) {
            if (cropLock[0].checked) {
                imgBox.addClass('disabled');
                loadImgAnc.remove();
                removeImgAnc.remove();
            } else {
                imgBox.removeClass('disabled');
                addControls();
            }
        })
    }

    Uploader.prototype.addCropData = function(imgBox, cb) {
        var cropDataItem = {};

        cropDataItem.imgBox = imgBox;
        cropDataItem.basic = imgBox.croppie({
            viewport: {
                width: 200,
                height: 150
            },
            enforceBoundary: true
        });

        cb(cropDataItem)

        cropData.push(cropDataItem);
    }

    Uploader.prototype.uploadCarData = function(imgBox, cropLock, activatorInput, eventType) {
        this.addCropData(imgBox, function (cropDataItem) {
            cropDataItem.basic.on('update.croppie', function (ev, cropData) {
                cropDataItem.imgPoints = cropData.points;
            })

            activatorInput.on(eventType, function(e) {
                cropDataItem.imgUrl = URL.createObjectURL(e.target.files[0]);
                cropDataItem.basic.croppie('bind', {
                    url: cropDataItem.imgUrl,
                });
                cropLock.attr('disabled', false)
            })
        })
    }

    Uploader.prototype.editCarData = function(imgBox, image) { 
        image.remove(); 
        this.addCropData(imgBox, function(cropDataItem) {
            cropDataItem.basic.on('update.croppie', function (ev, cropData) {
                cropDataItem.imgPoints = cropData.points;
            })

            cropDataItem.imgUrl = $(image).attr('src');
            cropDataItem.basic.croppie('bind', {
                url: cropDataItem.imgUrl,
                points: ["0", "0", "500", "375"]
            });
        })
    }

    Uploader.prototype.removeCropData = function(imgBox) {
        cropData = _.filter(cropData, function(cdi) {
            return cdi.imgBox !== imgBox
        })
    }

    Uploader.prototype.addToImageBuffer = function(imgBox, img) {
        imagesBuffer.push({ imgBox: imgBox, img: img });
    }
    
    Uploader.prototype.removeFromImageBuffer = function (imgBox, img) {
        if (imgBox) {
            imagesBuffer = _.filter(imagesBuffer, function (imgObject) {
                return imgObject.imgBox !== imgBox;
            });
        } else {
            if (img) {
                imagesBuffer = _.filter(imagesBuffer, function (imgObject) {
                    return imgObject.img !== img;
                });
            }
        }
    }

    Uploader.prototype.transferImageBufferToCropdata = function () {
        var self = this;
        _.forEach(imagesBuffer, function(imgObject) {
            self.addCropData(imgObject.imgBox, function (cropDataItem) {
                cropDataItem.imgUrl = imgObject.img[0].src;
                cropDataItem.imgPoints = ["0", "0", "500", "375"];
            })
            self.removeFromImageBuffer(imgObject.imgBox);
        })
    }

    return Uploader;
})()

// var uploader;

function postCarDetails(event) {
    uploader = event.data.uploader;
    event.preventDefault();
    uploader.post(event)
            .then(function(response) {
                $('#upload-form .fileUploadForm').parent().parent().html("<div class='row container fileUploadForm'><h5>Upload Image</h5><p>" + response + "</p></div>")
                setTimeout(() => {
                    location.reload();
                }, 500);
                uploader = null;
            })
}

function deleteCarDetails(event, uploader) {
    event.preventDefault();
    if (confirm("Are sure about deleting this car data?")) {
        var id = $(event.target).parent().find('input.private-data').val();
        uploader.delete(event, id)
                .then(function(data) {
                    location.reload();
                    window.alert(data.message);
                })
                .catch(function(data) {
                    console.log('Error!')
                    console.log(data);
                })
    }
}

function fillEditCarDetails(event, uploader) {
    event.preventDefault();
    uploader.get(event)
            .then(function (car) {
                $('#edit-car-details .title').val(car.description.title);
                $('#edit-car-details .year').val(car.description.year);
                $('#edit-car-details .make').val(car.description.make);
                $('#edit-car-details .model').val(car.description.model);
                $('#edit-car-details .state').val(car.description.state);
                $('#edit-car-details .condition').val(car.description.condition);
                $('#edit-car-details .price').val(car.description.price);
                $('#edit-car-details .mileage').val(car.description.mileage);
                $('#edit-car-details .ext-color').val(car.description.extColor);
                $('#edit-car-details .int-color').val(car.description.intColor);
                $('#edit-car-details .transmission').val(car.description.transmission);
                $('#edit-car-details .drivetrain').val(car.description.drivetrain);
                $('#edit-car-details .availability').val(car.description.availability);
                var container = $('#edit-car-details .preview-images')
                container.html(null);
                for (var i = 0; i < car.src.length; i++) {
                    // closure created because of src being passed to a callback; ES6 let could be used instead
                    ;(function () {
                        var imgUnit = $("<div class='img-unit center col s12 m12 l6 xl6'></div>");
                        var editLockWrapper = $("<label><input type='checkbox' /><span>Edit</span></label>");
                        var cropLockWrapper = $("<label><input type='checkbox' /><span>Lock</span></label>");
                        var removeImgAnc = $("<a class='modal-upload'>Remove</a>");
                        var imgBox = $("<div class='img-box'></div>");
                        imgUnit.append(cropLockWrapper);
                        imgUnit.append(imgBox);
                        var cropLock = cropLockWrapper.find('input');
                        var edtiLock = editLockWrapper.find('input')
                        var src = car.src[i];
                        var image = $("<img class='materialboxed col s12 m12 l12 xl12' src='" + src + ".webp' />");
                        image.on('error', function (event) {
                            this.src = src + '.jpg'
                        })
                        uploader.addToImageBuffer(imgBox, image);
                        function addControls() {
                            editLockWrapper.insertBefore(cropLockWrapper);
                            removeImgAnc.insertAfter(cropLockWrapper);
                            edtiLock.click(function () {
                                if (this.checked) {
                                    uploader.removeFromImageBuffer(imgBox);
                                    uploader.editCarData(imgBox, image);
                                } else {
                                    uploader.removeCropData(imgBox);
                                    imgBox.remove();
                                    imgBox = $("<div class='img-box'></div>");
                                    imgBox.append(image);
                                    imgUnit.append(imgBox);
                                    uploader.addToImageBuffer(imgBox, image);
                                }
                            })
                            removeImgAnc.click(function () {
                                uploader.removeCropData(imgBox);
                                uploader.removeFromImageBuffer(imgBox);
                                imgUnit.remove();
                            });
                        }
                        addControls();
                        imgBox.append(image);
                        container.append(imgUnit);

                        cropLock.click(function (event) {
                            if (this.checked) {
                                imgBox.addClass('disabled');
                                editLockWrapper.remove();
                                removeImgAnc.remove();
                            } else {
                                imgBox.removeClass('disabled');
                                addControls();
                            }
                        })
                    })(i);
                    $(document).ready(function () {
                        $('.materialboxed').materialbox();
                    });
                }
                $('#edit-car-details a.modal-load-img').click(uploader.addImgUnit.bind(uploader));
                $("#edit-car-details .submit-btn").click(function (event) {
                    uploader.transferImageBufferToCropdata()
                    uploader.put(event, car.id);
                });
            })
}

function fillCarDetails(event, uploader) {
    event.preventDefault();
    uploader.get(event)
            .then(function(car) {
                $('#car-details li.title span').html(car.description.title);
                $('#car-details li.year span').html(car.description.year);
                $('#car-details li.make span').html(car.description.make);
                $('#car-details li.model span').html(car.description.model);
                $('#car-details li.state span').html(car.description.state);
                $('#car-details li.condition span').html(car.description.condition);
                $('#car-details li.price span').html(new Intl.NumberFormat('USD', { style: 'currency', currency: 'USD' }).format(car.description.price));
                $('#car-details li.mileage span').html(new Intl.NumberFormat({ style: 'decimal' }).format(car.description.mileage) + ' mi');
                $('#car-details li.ext-color span').html(car.description.extColor);
                $('#car-details li.int-color span').html(car.description.intColor);
                $('#car-details li.transmission span').html(car.description.transmission);
                $('#car-details li.drivetrain span').html(car.description.drivetrain);
                $('#car-details li.availability span').html(car.description.availability);
                var container = $('#car-details .preview-images')
                container.html(null);
                for (var i = 0; i < car.src.length; i++) {
                    // closure created because of src being passed to a callback; ES6 let could be used instead
                    (function () {
                        var src = car.src[i];
                        var image = $("<img class='materialboxed col s6 m6 l4 xl4' src='" + src + ".webp' />");
                        image.on('error', function (event) {
                            this.src = src + '.jpg'
                        })
                        container.append(image);
                        $(document).ready(function () {
                            $('.materialboxed').materialbox();
                        });
                    })(i)
                }
            })
}

$('#upload-form-btn').click(function (event) {
    var form = $('#upload-form .descriptionForm')[0];
    var previewImages = $('#upload-form .preview-images');

    var uploader = new Uploader(form, previewImages);
    $("#upload-form .modal-load-img").click(uploader.addImgUnit.bind(uploader));
    $("#upload-form .submit-btn").click({ uploader: uploader }, postCarDetails);
})

$('a.delete').click(function (event) {
    var uploader = new Uploader(null, null);
    deleteCarDetails(event, uploader)
});

$('a.learn-more').click(function (event) {
    var form = $('#car-details .descriptionForm')[0];
    var previewImages = $('#car-details .preview-images');

    var uploader = new Uploader(form, previewImages);
    fillCarDetails(event, uploader);
});

$('a.edit').click(function (event) {
    var form = $('#edit-car-details .descriptionForm')[0];
    var previewImages = $('#edit-car-details .preview-images');

    var uploader = new Uploader(form, previewImages);
    fillEditCarDetails(event, uploader);
});

/* ------------------------------ Parallax ------------------------------
------------------------------------------------------------------------- */

document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.parallax');
    var instances = M.Parallax.init(elems, { responsiveThreshold: 0 });
});

/* ------------------------------ Services ------------------------------
------------------------------------------------------------------------- */

document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.carousel');
    var instances = M.Carousel.init(elems, {
        fullWidth: true,
    });
    var left = document.getElementsByClassName("left");
    for (let i = 0; i < left.length; i++) left[i].addEventListener("click", function() {
        instances[i].prev();
    });
    var right = document.getElementsByClassName("right");
    for (let j = 0; j < right.length; ++j) right[j].addEventListener("click", function () {
        instances[j-1].next();
    });
});

