/* ----------------------------- General -----------------------------
------------------------------------------------------------------------- */
AOS.init();

/* ----------------------------- Navigation -----------------------------
------------------------------------------------------------------------- */

var imgs = $('img.materialboxed');
var final = _.map(imgs, function(img) {
    return $(img).height();
})
// alert(final)

if (!window.localStorage) {
    $("header").append("<div style='text-align: center; margin: 0; padding: 10px 0px; position: absolute; top: 64px; width: 100%; color: white; background-color: red; z-index: 105;'><h5 style='margin: 0'>Please switch to a browser with a better support for HTML, JavaScript and CSS, Thanks!</h5></div>")
}

document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {
        edge: 'left',
        draggable: true,
        preventScrolling: true
    });
    elems[0].addEventListener('click', function(e) {
        var instance = M.Sidenav.getInstance(this);
        instance.close()
    })
});

// ----- Tabs ------

var el = document.getElementById('tabs-swipe')
var instance = M.Tabs.init(el, {
    swipeable: false
});

function menuAction(str) {
    localStorage.setItem('activeTab', str);
    onPageReload();
    window.scrollTo(0, 0);
}

function onPageReload() {
    var tab = localStorage.getItem('activeTab') || 'home';
    instance.select(tab);
    $('.slides img').css('src', 'none')
    $('#mobile a, #desktop a').removeClass('active');
    switch(tab) {
        case 'home':
            $('#mobile li.home a, #desktop li.home a').addClass('active');
            break;
        case 'about':
            $('#mobile li.about a, #desktop li.about a').addClass('active');
            break;
        case 'services':
            $('#mobile li.services a, #desktop li.services a').addClass('active');
            break;
        case 'car-sale':
            $('#mobile li.gallery a, #desktop li.gallery a').addClass('active');
    } 
    resizeCards('#' + tab);
    AOS.init();
    return false;
}

function resizeCards(tab) {
    var cardImages = $(tab + ' .card-image img');
    var cardDescriptions = $(tab + ' .card-image + .card-content');
    cardImages.css('height', 'auto');
    cardDescriptions.css('height', 'auto')
    if (window.innerWidth > 600) {
        resizeEven(cardImages);
        resizeEven(cardDescriptions);
    } else {
        // resizeEven(cardImages);
        resizeAuto(cardDescriptions);
    }
}

function resizeEven(elems) {
    var cardsHeights = _.map(elems, function (item) {
        return $(item).height();
    })
    var maxHeight = _.max(cardsHeights);
    // alert(maxHeight);
    (maxHeight > 0) ? elems.height(maxHeight) : elems.height(170);
}

function resizeAuto(elems) {
    $(elems).css({'height': 'auto'})
}

onPageReload();

$(window).resize(function (e) {
    // location.reload(); 
    onPageReload()
})

document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.materialboxed');
    var instances = M.Materialbox.init(elems);
});

// window.addEventListener('orientationchange', function(event) {
//     if (window.matchMedia("(orientation: landscape)").matches) {
        
//     }
// })

/* ------------------------ Home - Welcome Slider -----------------------
------------------------------------------------------------------------- */

document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.slider');
    var instances = M.Slider.init(elems, {
        duration: 500,
        interval: 5000,
        height: 400
    });
});

// var hasWebP = true;
// (function() {
//     var img = new Image();
//     img.onerror = function() {
//         hasWebP = false;
//         $('.slider img').each(function() {
//             var backgroundStr = "background-image: url(" + this.dataset.src + ".jpg)"
//             $(this).attr('style', backgroundStr)
//         })
//     }
//     img.src = '/images/slider/slider1.webp'
// })();

(function () {
    var img = new Image();
    img.onerror = function () {
        $('.slider img').each(function () {
            var backgroundStr = "background-image: url(" + this.dataset.src + ".jpg)"
            $(this).attr('style', backgroundStr)
        });
        $('body').attr('style', "background: url('images/backgrounds/main-bg.jpg') no-repeat top right / cover");
        $('footer.page-footer').attr('style', "background: url('/images/backgrounds/footer-bg.jpg') no-repeat top right / cover");
    }
    img.src = '/images/slider/slider1.webp'
})();
/* ------------------------------ Modal - Email ------------------------------
------------------------------------------------------------------------- */

var modalInstance;
document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.modal');
    modalInstance = M.Modal.init(elems, {
        startingTop: '1%',
        endingTop: "20%",
        onOpenEnd: function() {
            $('#emailForm').html(`
            <input class="validate" id="email" type="email" placeholder="enter email" name="email"/>
            <textarea id="message" name="message" placeholder="your message"></textarea>
            <button class="waves-effect waves-light btn grey darken-3 btn-flat grey-text" id="send" onclick="sendEmail(event);false;" type="button">Send</button>
            <button class="waves-effect waves-light btn-small grey darken-3 btn-flat grey-text" id="clear" onclick="clearFields(event);false;" type="button">Clear</button>`);
            $email = $('#email');
            $message = $('#message');
            $email.on('change', function () {
                console.log($(this).val())
                localStorage.setItem('email', $(this).val());
            }).on('keyup', function () {
                console.log($(this).val())
                localStorage.setItem('email', $(this).val());
            });
            $message.on('change', function () {
                localStorage.setItem('message', $(this).val());
            }).on('keyup', function () {
                localStorage.setItem('message', $(this).val());
            });
            var email = localStorage.getItem('email');
            var message = localStorage.getItem('message');
            if (email !== "") $email.val(email);
            if (message !== "") $message.val(message);
        }
    });
});


document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.fixed-action-btn');
    var instances = M.FloatingActionButton.init(elems, {
        direction: 'left'
    });
});

function sendEmail() {
    var email = {
        email: $("#email").val(),
        message: $("#message").val()
    }
    $.post('/mail', email).done(() => {
        $('#emailForm').html('<p>Your message was sent successfully!<br>We will contact you soon.<br>Please give us also a call if you do not hear from us!</p>')
        localStorage.setItem('email', "");
        localStorage.setItem('message', "");
    });
}

function clearFields() {
    $('#email').val(null);
    $('#message').val(null);
    localStorage.setItem('email', "");
    localStorage.setItem('message', "");
}

/* ------------------------ Image Upload Preview ------------------------
------------------------------------------------------------------------- */
var cropData = [];

function addImgUnit() {
    var imgUnit = $("<div class='img-unit center col s12 m12 l6 xl6'></div>");
    var uploadI = $("<input class='upload-input grey darken-3 btn-flat m-1 waves-effect waves-light grey-text' name='job-picture' type='file' />");
    var imgB = $("<div class='img-box'> </div>");
    imgUnit.append(uploadI);
    imgUnit.append(imgB);
    $('#previewImages').append(imgUnit);

    uploadCarData(imgB, uploadI);
}

function uploadCarData(imgBox, uploadInput) {
    var cropDataItem = {};
    cropDataItem.basic = imgBox.croppie({
        viewport: {
            width: 200,
            height: 150
        },
        enforceBoundary: true
    });

    basic.on('update.croppie', function (ev, cropData) {
        cropDataItem.imgPoints = cropData.points;
    })

    uploadInput.on('change', function (e) {
        cropDataItem.imgUrl = URL.createObjectURL(e.target.files[0]);
        basic.croppie('bind', {
            url: cropDataItem.imgUrl,
        });
    })

    cropData.push(cropDataItem);
}

$("#submit-btn").on('click', function (event) {
    event.preventDefault();
    var data = new FormData($('#descriptionForm')[0]);

    var deferreds = cropData.map(cdi => {
        cdi.basic.croppie('bind', {
            url: cdi.imgUrl,
            points: cdi.imgPoints
        }).then(function () {
            return basic.croppie('result', {
                type: 'blob',
                size: { width: 500 },
                format: 'jpeg',
                quality: 1
            })
        }).then(function (blob) {
            data.append('job-picture', blob, 'image.jpg');
        })
    })

    $.when.apply($, deferreds).then(function() {
        $.ajax({
            type: "post",
            enctype: 'multipart/form-data',
            url: '/upload',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            success: function (response) {
                console.log(response)
            },
            error: function (response) {
                console.log(response)
            }
        })
    })
    
})

// var imgB = $('.img-box');
// var uploadI = $('.upload-input');

// uploadCarData(imgB, uploadI);

/* ------------------------------ Parallax ------------------------------
------------------------------------------------------------------------- */

document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.parallax');
    var instances = M.Parallax.init(elems, { responsiveThreshold: 0 });
});

/* ------------------------------ Services ------------------------------
------------------------------------------------------------------------- */




