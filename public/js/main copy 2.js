/* ----------------------------- General -----------------------------
------------------------------------------------------------------------- */
AOS.init();

/* ----------------------------- Navigation -----------------------------
------------------------------------------------------------------------- */

var imgs = $('img.materialboxed');
var final = _.map(imgs, function(img) {
    return $(img).height();
})
// alert(final)

if (!window.localStorage) {
    $("header").append("<div style='text-align: center; margin: 0; padding: 10px 0px; position: absolute; top: 64px; width: 100%; color: white; background-color: red; z-index: 105;'><h5 style='margin: 0'>Please switch to a browser with a better support for HTML, JavaScript and CSS, Thanks!</h5></div>")
}

document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {
        edge: 'left',
        draggable: true,
        preventScrolling: true
    });
    elems[0].addEventListener('click', function(e) {
        var instance = M.Sidenav.getInstance(this);
        instance.close()
    })
});

// ----- Tabs ------
var el = document.getElementById('tabs-swipe')
var instance = M.Tabs.init(el, {
    swipeable: false
});

function menuAction(str) {
    localStorage.setItem('activeTab', str);
    if (localStorage.getItem('activeTab') === 'car-sale') location.reload()
    onPageReload();
    window.scrollTo(0, 0);
}

// function onPageReload() {
//     var tab = localStorage.getItem('activeTab') || 'home';
//     instance.select(tab);
//     $('.slides img').css('src', 'none')
//     $('#mobile a, #desktop a').removeClass('active');
//     switch(tab) {
//         case 'home':
//             $('#mobile li.home a, #desktop li.home a').addClass('active');
//             break;
//         case 'about':
//             $('#mobile li.about a, #desktop li.about a').addClass('active');
//             break;
//         case 'services':
//             $('#mobile li.services a, #desktop li.services a').addClass('active');
//             break;
//         case 'car-sale':
//             $('#mobile li.gallery a, #desktop li.gallery a').addClass('active');
//     } 
//     resizeCards('#' + tab);
//     AOS.init();
//     return false;
// }

// function resizeCards(tab) {
//     var cardImages = $(tab + ' .card-image img');
//     var cardDescriptions = $(tab + ' .card-image + .card-content');
//     cardImages.css('height', 'auto');
//     cardDescriptions.css('height', 'auto')
//     if (window.innerWidth > 600) {
//         $(tab + ' .card-image').height(resizeEven(cardImages));
//         resizeEven(cardDescriptions);
//     } else {
//         $(tab + ' .card-image').height(resizeEven(cardImages));
//         resizeAuto(cardDescriptions);
//     }
// }

// function resizeEven(elems) {
//     var cardsHeights = _.map(elems, function (item) {
//         return $(item).height();
//     })
//     var maxHeight = _.max(cardsHeights);
//     var finaleMaxHeight = (maxHeight > 0) ? maxHeight : 170;
//     elems.height(finaleMaxHeight);
//     return finaleMaxHeight;
// }

// function resizeAuto(elems) {
//     $(elems).css({'height': 'auto'})
// }
debugger

var ResizeCardsTools = (function() {
    function ResizeCardsTools() {}

    ResizeCardsTools.prototype.resizeCards = function(cards) {

        var cardImages = cards.find('.card-image');
        var cardImageImgs = cards.find('img');
        var cardDescriptions = cards.find('.card-content')
        if (cardImages.length) {
            cardImageImgs.css('height', 'auto');
            if (window.innerWidth > 600) {
                cardImages.height(this.resizeEven(cardImageImgs));
            } else {
                cardImages.height(this.resizeEven(cardImageImgs));
            }
        }
        if (cardDescriptions.length) {
            cardDescriptions.css('height', 'auto')
            if (window.innerWidth > 600) {
                this.resizeEven(cardDescriptions);
            } else {
                this.resizeAuto(cardDescriptions);
            }
        }
        
    }

    ResizeCardsTools.prototype.resizeEven = function (elems) {
        var cardsHeights = _.map(elems, function (item) {
            return $(item).height();
        })
        var maxHeight = _.max(cardsHeights);
        var finaleMaxHeight = (maxHeight > 0) ? maxHeight : 170;
        elems.height(finaleMaxHeight);
        return finaleMaxHeight;
    }

    ResizeCardsTools.prototype.resizeAuto = function (elems) {
        $(elems).css({ 'height': 'auto' })
    }

    return ResizeCardsTools;
})()

var resizeCardsTools = new ResizeCardsTools();

function onPageReload() {
    var tab = localStorage.getItem('activeTab') || 'home';
    instance.select(tab);
    $('.slides img').css('src', 'none')
    $('#mobile a, #desktop a').removeClass('active');
    switch (tab) {
        case 'home':
            $('#mobile li.home a, #desktop li.home a').addClass('active');
            break;
        case 'about':
            $('#mobile li.about a, #desktop li.about a').addClass('active');
            break;
        case 'services':
            $('#mobile li.services a, #desktop li.services a').addClass('active');
            break;
        case 'car-sale':
            $('#mobile li.gallery a, #desktop li.gallery a').addClass('active');
    }
    var cards = $('#' + tab + ' .card.resizable');
    resizeCardsTools.resizeCards(cards);
    AOS.init();
    return false;
}

$(document).ready(function() {
    $(window).trigger('resize');
})

$(window).resize(function (e) {
    onPageReload()
})

document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.materialboxed');
    var instances = M.Materialbox.init(elems);
});

// window.addEventListener('orientationchange', function(event) {
//     if (window.matchMedia("(orientation: landscape)").matches) {
        
//     }
// })

/* ------------------------ Home - Welcome Slider -----------------------
------------------------------------------------------------------------- */

document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.slider');
    var instances = M.Slider.init(elems, {
        duration: 500,
        interval: 5000,
        height: 400
    });
});

// var hasWebP = true;
// (function() {
//     var img = new Image();
//     img.onerror = function() {
//         hasWebP = false;
//         $('.slider img').each(function() {
//             var backgroundStr = "background-image: url(" + this.dataset.src + ".jpg)"
//             $(this).attr('style', backgroundStr)
//         })
//     }
//     img.src = '/images/slider/slider1.webp'
// })();

(function () {
    var img = new Image();
    img.onerror = function () {
        $('.slider img').each(function () {
            var backgroundStr = "background-image: url(" + this.dataset.src + ".jpg)"
            $(this).attr('style', backgroundStr)
        });
        $('body').attr('style', "background: url('images/backgrounds/main-bg.jpg') no-repeat top right / cover");
        $('footer.page-footer').attr('style', "background: url('/images/backgrounds/footer-bg.jpg') no-repeat top right / cover");
    }
    img.src = '/images/slider/slider1.webp'
})();
/* ------------------------------ Modal - Email ------------------------------
------------------------------------------------------------------------- */

var modalInstance;
document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.modal');
    modalInstance = M.Modal.init(elems, {
        startingTop: '1%',
        endingTop: "20%",
        onOpenEnd: function() {
            $('#emailForm').html(`
            <input class="validate" id="email" type="email" placeholder="enter email" name="email"/>
            <textarea id="message" name="message" placeholder="your message"></textarea>
            <button class="waves-effect waves-light btn grey darken-3 btn-flat grey-text" id="send" onclick="sendEmail(event);false;" type="button">Send</button>
            <button class="waves-effect waves-light btn-small grey darken-3 btn-flat grey-text" id="clear" onclick="clearFields(event);false;" type="button">Clear</button>`);
            $email = $('#email');
            $message = $('#message');
            $email.on('change', function () {
                console.log($(this).val())
                localStorage.setItem('email', $(this).val());
            }).on('keyup', function () {
                console.log($(this).val())
                localStorage.setItem('email', $(this).val());
            });
            $message.on('change', function () {
                localStorage.setItem('message', $(this).val());
            }).on('keyup', function () {
                localStorage.setItem('message', $(this).val());
            });
            var email = localStorage.getItem('email');
            var message = localStorage.getItem('message');
            if (email !== "") $email.val(email);
            if (message !== "") $message.val(message);
        }
    });
});

document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.fixed-action-btn');
    var instances = M.FloatingActionButton.init(elems, {
        direction: 'left'
    });
});

function sendEmail() {
    var email = {
        email: $("#email").val(),
        message: $("#message").val()
    }
    $.post('/mail', email).done(() => {
        $('#emailForm').html('<p>Your message was sent successfully!<br>We will contact you soon.<br>Please give us also a call if you do not hear from us!</p>')
        localStorage.setItem('email', "");
        localStorage.setItem('message', "");
    });
}

function clearFields() {
    $('#email').val(null);
    $('#message').val(null);
    localStorage.setItem('email', "");
    localStorage.setItem('message', "");
}

/* ------------------------ Image Upload Preview ------------------------
------------------------------------------------------------------------- */

var Uploader = (function() {
    cropData = [];

    function Uploader(submitBtn, previewImages) {
        this.previewImages = previewImages;

        submitBtn.on('click', function (event) {
            event.preventDefault();
            var data = new FormData($('#descriptionForm')[0]);

            var deferreds = _.map(cropData, cdi => {
                var deferred = $.Deferred();
                cdi.basic.croppie('bind', {
                    url: cdi.imgUrl,
                    points: cdi.imgPoints
                }).then(function () {
                    return cdi.basic.croppie('result', {
                        type: 'blob',
                        size: { width: 500 },
                        format: 'jpeg',
                        quality: 1
                    })
                }).then(function (blob) {
                    data.append('job-picture', blob, 'image.jpg');
                    deferred.resolve(data);
                })
                return deferred;
            })

            $.when.apply($, deferreds).done(function (data) {
                $.ajax({
                    type: "post",
                    enctype: 'multipart/form-data',
                    url: '/upload',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (response) {
                        $('#fileUploadForm').html("<div class='row container' id='fileUploadForm'><h5>Upload Image</h5><p>" + response + "</p></div>")
                        setTimeout(() => {
                            location.reload();
                        }, 2000);
                    },
                    error: function (response) {
                        console.log(response)
                    }
                })
            })

        })
    }

    Uploader.prototype.addImgUnit = function (e) {
        var imgUnit = $("<div class='img-unit center col s12 m12 l6 xl6'></div>");
        var loadImgAnc = $("<a class='modal-upload'>Load</a>");
        var cropLockWrapper = $("<label><input type='checkbox' disabled='disabled' /><span>Lock</span></label>");
        var removeImgAnc = $("<a class='modal-upload'>Remove</a>");
        var uploadInput = $("<input class='upload-input grey darken-3 btn-flat m-1 waves-effect waves-light grey-text' name='job-picture' type='file' />");
        var imgBox = $("<div class='img-box'></div>");
        imgUnit.append(loadImgAnc);
        imgUnit.append(cropLockWrapper);
        imgUnit.append(removeImgAnc);
        imgUnit.append(uploadInput);
        imgUnit.append(imgBox);
        this.previewImages.append(imgUnit);

        loadImgAnc.click(function () {
            uploadInput.click();
        })
        removeImgAnc.click(function () {
            imgUnit.remove()
            removeCardData.call(this, imgBox)
        })

        var cropLock = cropLockWrapper.find('input');
        uploadCarData.call(this, imgBox, uploadInput, cropLock);
    }

    function uploadCarData(imgBox, uploadInput, cropLock) {
        var cropDataItem = {};

        cropDataItem.imgBox = imgBox;
        cropDataItem.basic = imgBox.croppie({
            viewport: {
                width: 200,
                height: 150
            },
            enforceBoundary: true
        });

        cropDataItem.basic.on('update.croppie', function (ev, cropData) {
            cropDataItem.imgPoints = cropData.points;
        })

        uploadInput.on('change', function (e) {
            cropDataItem.imgUrl = URL.createObjectURL(e.target.files[0]);
            cropDataItem.basic.croppie('bind', {
                url: cropDataItem.imgUrl,
            });
            cropLock.attr('disabled', false)
            debugger
            cropLock.click(function () {
                if (!this.disabled) {
                    if (this.checked) {
                        imgBox.addClass('disabled')
                    } else {
                        imgBox.removeClass('disabled')
                    }
                }
            })
        })

        cropData.push(cropDataItem);
    }

    function removeCardData(imgBox) {
        cropData = _.filter(cropData, function (cropDataItem) {
            return cropDataItem.imgBox !== imgBox;
        })
    }

    return Uploader;
})()

var submitBtn = $("#submit-btn");
var previewImages = $('#preview-images');

var uploader = new Uploader(submitBtn, previewImages);


/* ------------------------ Image Upload Preview ------------------------
------------------------------------------------------------------------- */

// var cropData = [];

// function addImgUnit() {
//     var imgUnit = $("<div class='img-unit center col s12 m12 l6 xl6'></div>");
//     var loadImgAnc = $("<a class='modal-upload'>Load</a>");
//     var cropLockWrapper = $("<label><input type='checkbox' disabled='disabled' /><span>Lock</span></label>");
//     var removeImgAnc = $("<a class='modal-upload'>Remove</a>");
//     var uploadInput = $("<input class='upload-input grey darken-3 btn-flat m-1 waves-effect waves-light grey-text' name='job-picture' type='file' />");
//     var imgBox = $("<div class='img-box'></div>");
//     imgUnit.append(loadImgAnc);
//     imgUnit.append(cropLockWrapper);
//     imgUnit.append(removeImgAnc);
//     imgUnit.append(uploadInput);
//     imgUnit.append(imgBox);
//     $('#preview-images').append(imgUnit);

//     loadImgAnc.click(function() {
//         uploadInput.click();
//     })
//     removeImgAnc.click(function() {
//         imgUnit.remove()
//         removeCardData(imgBox)
//     })

//     var cropLock = cropLockWrapper.find('input');
//     uploadCarData(imgBox, uploadInput, cropLock);
// }

// function uploadCarData(imgBox, uploadInput, cropLock) {
//     var cropDataItem = {};

//     cropDataItem.imgBox = imgBox;
//     cropDataItem.basic = imgBox.croppie({
//         viewport: {
//             width: 200,
//             height: 150
//         },
//         enforceBoundary: true
//     });

//     cropDataItem.basic.on('update.croppie', function (ev, cropData) {
//         cropDataItem.imgPoints = cropData.points;
//     })

//     uploadInput.on('change', function (e) {
//         cropDataItem.imgUrl = URL.createObjectURL(e.target.files[0]);
//         cropDataItem.basic.croppie('bind', {
//             url: cropDataItem.imgUrl,
//         });
//         cropLock.attr('disabled', false)
//         debugger
//         cropLock.click(function() {
//             if (!this.disabled) {
//                 if (this.checked) {
//                     imgBox.addClass('disabled')
//                 } else { 
//                     imgBox.removeClass('disabled') 
//                 }
//             }
//         })
//     })

//     cropData.push(cropDataItem);
// }

// function removeCardData(imgBox) {
//     cropData = _.filter(cropData, function(cropDataItem) {
//         return cropDataItem.imgBox !== imgBox;
//     })
// }

// $("#submit-btn").on('click', function (event) {
//     event.preventDefault();
//     var data = new FormData($('#descriptionForm')[0]);

//     var deferreds = cropData.map(cdi => {
//         var deferred = $.Deferred();
//         cdi.basic.croppie('bind', {
//             url: cdi.imgUrl,
//             points: cdi.imgPoints
//         }).then(function () {
//             return cdi.basic.croppie('result', {
//                 type: 'blob',
//                 size: { width: 500 },
//                 format: 'jpeg',
//                 quality: 1
//             })
//         }).then(function (blob) {
//             data.append('job-picture', blob, 'image.jpg');
//             deferred.resolve(data);
//         })
//         return deferred;
//     })

//     $.when.apply($, deferreds).done(function(data) {
//         $.ajax({
//             type: "post",
//             enctype: 'multipart/form-data',
//             url: '/upload',
//             data: data,
//             processData: false,
//             contentType: false,
//             cache: false,
//             success: function (response) {
//                 $('#fileUploadForm').html("<div class='row container' id='fileUploadForm'><h5>Upload Image</h5><p>" + response + "</p></div>")
//                 setTimeout(() => {
//                     location.reload();
//                 }, 2000);
//             },
//             error: function (response) {
//                 console.log(response)
//             }
//         })
//     })
    
// })

/* ------------------------------ Parallax ------------------------------
------------------------------------------------------------------------- */

document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.parallax');
    var instances = M.Parallax.init(elems, { responsiveThreshold: 0 });
});

/* ------------------------------ Services ------------------------------
------------------------------------------------------------------------- */

document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.carousel');
    var instances = M.Carousel.init(elems, {
        fullWidth: true,
    });
    var left = document.getElementsByClassName("left");
    for (let i = 0; i < left.length; i++) left[i].addEventListener("click", function() {
        instances[i].prev();
    });
    var right = document.getElementsByClassName("right");
    for (let j = 0; j < right.length; ++j) right[j].addEventListener("click", function () {
        instances[j-1].next();
    });
});

