export class ResizeCardsTools {
  resizeCards(cards) {

    let cardImages = cards.find('.card-image');
    let cardImageImgs = cards.find('img');
    let cardDescriptions = cards.find('.card-content')
    if (cardImages.length) {
      cardImageImgs.css('height', 'auto');
      if (window.innerWidth > 600) {
        cardImages.height(this.resizeEven(cardImageImgs));
      } else {
        cardImages.height(this.resizeEven(cardImageImgs));
      }
    }
    if (cardDescriptions.length) {
      cardDescriptions.css('height', 'auto')
      if (window.innerWidth > 600) {
        this.resizeEven(cardDescriptions);
      } else {
        this.resizeAuto(cardDescriptions);
      }
    }
  }

  resizeEven(elems) {
    let cardsHeights = elems.map(item => $(item).height())
    let maxHeight = Math.max(cardsHeights);
    let finaleMaxHeight = (maxHeight > 0) ? maxHeight : 170;
    elems.height(finaleMaxHeight);
    return finaleMaxHeight;
  }

  resizeAuto(elems) {
    $(elems).css({ 'height': 'auto' })
  }
}