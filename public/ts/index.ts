import { Uploader } from './uploader';
import { ResizeCardsTools } from './resizecardstools';
import $ from 'jquery'
import M from "materialize-css"
import * as AOS from 'aos';

/* ----------------------------- General -----------------------------
------------------------------------------------------------------------- */
AOS.init();

/* ----------------------------- Navigation -----------------------------
------------------------------------------------------------------------- */

let imgs = $('img.materialboxed');
let final = imgs.map((img:any) => $(img).height())
// alert(final)

if (!window.localStorage) {
  $("header").append("<div style='text-align: center; margin: 0; padding: 10px 0px; position: absolute; top: 64px; width: 100%; color: white; background-color: red; z-index: 105;'><h5 style='margin: 0'>Please switch to a browser with a better support for HTML, JavaScript and CSS, Thanks!</h5></div>")
}

document.addEventListener('DOMContentLoaded', function() {
  let elems = document.querySelectorAll('.sidenav');
  let instances = M.Sidenav.init(elems, {
    edge: 'left',
    draggable: true,
    preventScrolling: true
  });
  elems[0].addEventListener('click', function (e) {
    let instance = M.Sidenav.getInstance(this);
    instance.close()
  })
});

// ----- Tabs ------

let resizeCardsTools = new ResizeCardsTools();

let el = document.getElementById('tabs-swipe')
let instance = M.Tabs.init(el, {
  swipeable: false
});

function menuAction(str) {
  localStorage.setItem('activeTab', str);
  if (localStorage.getItem('activeTab') === 'car-sales') location.reload()
  onPageReload();
  window.scrollTo(0, 0);
}

function onPageReload() {
  let tab = localStorage.getItem('activeTab') || 'home';
  instance.select(tab);
  $('.slides img').css('src', 'none')
  $('#mobile a, #desktop a').removeClass('active');
  switch (tab) {
    case 'home':
      $('#mobile li.home a, #desktop li.home a').addClass('active');
      break;
    case 'about':
      $('#mobile li.about a, #desktop li.about a').addClass('active');
      break;
    case 'services':
      $('#mobile li.services a, #desktop li.services a').addClass('active');
      break;
    case 'car-sales':
      $('#mobile li.gallery a, #desktop li.gallery a').addClass('active');
  }
  let cards = $('#' + tab + ' .card.resizable');
  resizeCardsTools.resizeCards(cards);
  AOS.init();
  return false;
}

$(document).ready(function () {
  $(window).trigger('resize');
})

$(window).resize(function (e) {
  onPageReload()
})

document.addEventListener('DOMContentLoaded', function () {
  let elems = document.querySelectorAll('.materialboxed');
  let instances = M.Materialbox.init(elems);
});

/* ------------------------ Home - Welcome Slider -----------------------
------------------------------------------------------------------------- */

document.addEventListener('DOMContentLoaded', function () {
  let elems = document.querySelectorAll('.slider');
  let instances = M.Slider.init(elems, {
    duration: 500,
    interval: 5000,
    height: 400
  });
});

(function () {
  let img = new Image();
  img.onerror = function () {
    $('.slider img').each(function () {
      let backgroundStr = "background-image: url(" + this.dataset.src + ".jpg)"
      $(this).attr('style', backgroundStr)
    });
    $('body').attr('style', "background: url('images/backgrounds/main-bg.jpg') no-repeat top right / cover");
    $('footer.page-footer').attr('style', "background: url('/images/backgrounds/footer-bg.jpg') no-repeat top right / cover");
  }
  img.src = '/images/slider/slider1.webp'
})();
/* ------------------------------ Modal - Email ------------------------------
------------------------------------------------------------------------- */

let modalInstance;
document.addEventListener('DOMContentLoaded', function () {
  let elems = document.querySelectorAll('.modal');
  modalInstance = M.Modal.init(elems, {
    startingTop: '1%',
    endingTop: "20%",
    onOpenEnd: function () {
      $('#emailForm').html(`
            <input class="validate" id="email" type="email" placeholder="enter email" name="email"/>
            <textarea id="message" name="message" placeholder="your message"></textarea>
            <button class="waves-effect waves-light btn grey darken-3 btn-flat grey-text" id="send" onclick="sendEmail(event);false;" type="button">Send</button>
            <button class="waves-effect waves-light btn-small grey darken-3 btn-flat grey-text" id="clear" onclick="clearFields(event);false;" type="button">Clear</button>`);
      let $email = $('#email');
      let $message = $('#message');
      $email.on('change', function () {
        console.log($(this).val())
        localStorage.setItem('email', $(this).val());
      }).on('keyup', function () {
        console.log($(this).val())
        localStorage.setItem('email', $(this).val());
      });
      $message.on('change', function () {
        localStorage.setItem('message', $(this).val());
      }).on('keyup', function () {
        localStorage.setItem('message', $(this).val());
      });
      let email = localStorage.getItem('email');
      let message = localStorage.getItem('message');
      if (email !== "") $email.val(email);
      if (message !== "") $message.val(message);
    }
  });
});

document.addEventListener('DOMContentLoaded', function () {
  let elems = document.querySelectorAll('.fixed-action-btn');
  let instances = M.FloatingActionButton.init(elems, {
    direction: 'left'
  });
});

function sendEmail() {
  let email = {
    email: $("#email").val(),
    message: $("#message").val()
  }
  $.post('/mail', email).done(() => {
    $('#emailForm').html('<p>Your message was sent successfully!<br>We will contact you soon.<br>Please give us also a call if you do not hear from us!</p>')
    localStorage.setItem('email', "");
    localStorage.setItem('message', "");
  });
}

function clearFields() {
  $('#email').val(null);
  $('#message').val(null);
  localStorage.setItem('email', "");
  localStorage.setItem('message', "");
}

/* ------------------------ Image Upload Preview ------------------------
------------------------------------------------------------------------- */
let form = $('#descriptionForm')[0];
let previewImages = $('#preview-images');

let uploader = new Uploader(form, previewImages);
$("#submit-btn").on('click', uploader.post.bind(uploader))
$('a.delete').click(uploader.delete.bind(uploader))

/* ------------------------------ Parallax ------------------------------
------------------------------------------------------------------------- */

document.addEventListener('DOMContentLoaded', function () {
  let elems = document.querySelectorAll('.parallax');
  let instances = M.Parallax.init(elems, { responsiveThreshold: 0 });
});

/* ------------------------------ Services ------------------------------
------------------------------------------------------------------------- */

document.addEventListener('DOMContentLoaded', function () {
  let elems = document.querySelectorAll('.carousel');
  let instances = M.Carousel.init(elems, {
    fullWidth: true,
  });
  let left = document.getElementsByClassName("left");
  for (let i = 0; i < left.length; i++) left[i].addEventListener("click", function () {
    instances[i].prev();
  });
  let right = document.getElementsByClassName("right");
  for (let j = 0; j < right.length; ++j) right[j].addEventListener("click", function () {
    instances[j - 1].next();
  });
});

