interface ICropDataItem {
  basic: any,
  imgBox: any,
  imgUrl: any,
  imgPoints: any
}

export class Uploader {
  private cropData: ICropDataItem[] = [];
  private form;
  private previewImages;

  constructor(form, previewImages) {
    this.form = form;
    this.previewImages = previewImages;
  }

  post(event) {
    event.preventDefault();
    let data = new FormData(this.form);

    let deferreds = _.map(this.cropData, cdi => {
      let deferred = $.Deferred();
      cdi.basic.croppie('bind', {
        url: cdi.imgUrl,
        points: cdi.imgPoints
      }).then(function () {
        return cdi.basic.croppie('result', {
          type: 'blob',
          size: { width: 500 },
          format: 'jpeg',
          quality: 1
        })
      }).then(function (blob) {
        data.append('job-picture', blob, 'image.jpg');
        deferred.resolve(data);
      })
      return deferred;
    })

    $.when.apply($, deferreds).done(function (data) {
      $.ajax({
        type: "post",
        url: '/upload',
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        success: function (response) {
          $('#fileUploadForm').parent().parent().html("<div class='row container' id='fileUploadForm'><h5>Upload Image</h5><p>" + response + "</p></div>")
          setTimeout(() => {
            location.reload();
          }, 1000);
        },
        error: function (response) {
          console.log(response)
        }
      })
    })
  }

  delete(event) {
    event.preventDefault();
    $.ajax({
      method: "delete",
      url: "/upload",
      data: { "delId": $(event.target).parent().find('input.private-data').val() },
      dataType: "json",
      success: function (data) {
        console.log('Sucess!')
        location.reload();
        window.alert(data.message);
      },
      error: function (data) {
        console.log('Error!')
        console.log(data.status);
      }
    });
  }

  addImgUnit(event) {
    let imgUnit = $("<div class='img-unit center col s12 m12 l6 xl6'></div>");
    let loadImgAnc = $("<a class='modal-upload'>Load</a>");
    let cropLockWrapper = $("<label><input type='checkbox' disabled='disabled' /><span>Lock</span></label>");
    let removeImgAnc = $("<a class='modal-upload'>Remove</a>");
    let uploadInput = $("<input class='upload-input grey darken-3 btn-flat m-1 waves-effect waves-light grey-text' name='job-picture' type='file' />");
    let imgBox = $("<div class='img-box'></div>");
    imgUnit.append(loadImgAnc);
    imgUnit.append(cropLockWrapper);
    imgUnit.append(removeImgAnc);
    imgUnit.append(uploadInput);
    imgUnit.append(imgBox);
    this.previewImages.append(imgUnit);

    loadImgAnc.click(function () {
      uploadInput.click();
    })
    removeImgAnc.click(() => {
      imgUnit.remove();
      this.removeCardData(imgBox);
    })

    let cropLock = cropLockWrapper.find('input');
    this.uploadCarData(imgBox, uploadInput, cropLock);
  }

  private uploadCarData(imgBox, uploadInput, cropLock) {
    let cropDataItem: ICropDataItem = {
      basic: null,
      imgBox: null,
      imgUrl: null,
      imgPoints: null
    };

    cropDataItem.imgBox = imgBox;
    cropDataItem.basic = imgBox.croppie({
      viewport: {
        width: 200,
        height: 150
      },
      enforceBoundary: true
    });

    cropDataItem.basic.on('update.croppie', function (ev, cropData) {
      cropDataItem.imgPoints = cropData.points;
    })

    uploadInput.on('change', function (e) {
      cropDataItem.imgUrl = URL.createObjectURL(e.target.files[0]);
      cropDataItem.basic.croppie('bind', {
        url: cropDataItem.imgUrl,
      });
      cropLock.attr('disabled', false)
      cropLock.click(function () {
        if (!this.disabled) {
          if (this.checked) {
            imgBox.addClass('disabled')
          } else {
            imgBox.removeClass('disabled')
          }
        }
      })
    })

    this.cropData.push(cropDataItem);
  }

  private removeCardData(imgBox) {
    this.cropData = this.cropData.filter(cropDataItem => cropDataItem.imgBox !== imgBox)
  }
}