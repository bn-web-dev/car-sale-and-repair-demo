const fs = require('fs');
module.exports = router = require('express').Router();

router.get('/upload/:id', (req, res) => {
  fs.readFile('./data/jobs.json', 'utf-8', (err, data) => {
    let result = JSON.parse(data).find(job => job.id === req.params.id);
    res.send(JSON.stringify(result));
  })
})