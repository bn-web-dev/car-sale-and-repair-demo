let passport = require('passport');
let localStrategy = require('passport-local').Strategy;
let users = require('./users.json')

passport.use(new localStrategy((username, password, done) => {
    let user = users.find(u => u.name === username)
    if (!user || user.password !== password) {
        done(null, false);
        return;
    }
    done(null, user)
}))

passport.serializeUser((user, done) => {
    done(null, user);
});

passport.deserializeUser((user, done) => {
    done(null, user);
});