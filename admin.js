const fs = require('fs')
module.exports = router = require('express').Router();

router.get('/admin', (req, res) => {
    fs.readFile('./data/jobs.json', 'utf-8', (err, data) => {
        res.render('admin', { jobs: JSON.parse(data) });
    })
})