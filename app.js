const path = require('path')
const fs = require('fs');
const express = require("express");
const compression = require('compression')
const thumb = require('thumb-express');
const bodyParser = require('body-parser');
const passport = require('passport');
const flash = require('connect-flash');
const expressSession = require('express-session');
const robots = require('express-robots-txt');

const port = process.env.PORT || 3000;
const app = express();

require('./passport-init');

app.use(robots({ UserAgent: '*', Disallow: '/temp' }))
   .use(compression())
   .use(thumb.init(path.join(__dirname, 'public')))
   .use(express.static("public"))
   .use(express.static("node_modules/"))
   .set("views", `${__dirname}/views`)
   .set("view engine", "pug");

app.use(bodyParser.json())
   .use(bodyParser.urlencoded({ extended: true }))
   .use(require('./mail'))
   .use(flash())
   .use(expressSession({
      secret: 'keyboard cat', resave: false, saveUninitialized: false
   }))
   .use(passport.initialize())
   .use(passport.session())
   .use(require('./auth'))
   .use(require('./api'))
   .get('/', (req, res) => {
      fs.readFile('./data/jobs.json', 'utf-8', (err, data) => {
         res.render('index', { jobs: JSON.parse(data) });
      })
   })
   .use((req, res, next) => {
      if (req.url === '/admin' ||
          req.url === '/upload' ||
          req.url === '/login') next()
      else {
         res.status(404)
         res.render('error-page');
      }
   })
   .use((req, res, next) => {
      if (req.isAuthenticated()) {
         res.locals.user = req.user;
         next()
         return;
      }
      res.redirect('/');
   })
   .use(require('./admin'))
   .use(require('./upload'))
   // .use(function (error, req, res, next) {
   //    res.send('500: Internal Server Error', 500);
   // });

app.listen(port, function () {
   console.log(`My Auto Repair & Service Center, listening on port ${port}...!`);
});

// app.use(require('./logging'));

